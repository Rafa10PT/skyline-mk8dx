.arm
#ifndef __clang__
.fpu vfp
#endif
.syntax unified
.align(4);

.macro CODE_BEGIN name
    .section .text.\name, "ax", %progbits
    .global \name
    .type \name, %function
    .align 4
    .cfi_startproc
\name:
.endm

.macro CODE_END
    .cfi_endproc
.endm

.macro armBackupRegisters
    PUSH {R0-R12, LR}
.endm

.macro armRecoverRegisters
    POP {R0-R12, LR}
.endm


CODE_BEGIN inlineHandlerImpl
    // branch and link to hook callback
    LDR LR, [R12, #4]
    BLX LR

    // branch to trampoline
    LDR R12, [R12, #8]
    BX R12

CODE_END


.global inlineHandlerStart
.global inlineHandlerEnd

CODE_BEGIN inlineHandler
inlineHandlerStart:
    ADR R12, inlineHandlerEnd
    LDR LR, [R12]
    BX LR

CODE_END
inlineHandlerEnd:
