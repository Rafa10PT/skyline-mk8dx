#include "skyline/utils/ipc.hpp"

#include "skyline/utils/cpputils.hpp"

#ifdef __cplusplus
extern "C" {
#endif

#include "skyline/nx/sf/cmif.h"

#ifdef __cplusplus
}
#endif

namespace skyline::utils {

Result nnServiceCreate(Service* srv, const char* name) {
    // open session
    nn::sf::hipc::InitializeHipcServiceResolution();
    nn::svc::Handle svcHandle;
    nn::sf::hipc::ConnectToHipcService(&svcHandle, name);
    nn::sf::hipc::FinalizeHipcServiceResolution();

    void* base = nn::sf::hipc::GetMessageBufferOnTls();

    cmifMakeControlRequest(base, 3, 0);
    R_TRY(nn::sf::hipc::SendSyncRequest(svcHandle, base, 0x100));

    CmifResponse resp = {};
    R_TRY(cmifParseResponse(&resp, base, false, sizeof(u16)));

    // build srv obj
    srv->session = svcHandle.handle;
    srv->own_handle = 1;
    srv->object_id = 0;
    srv->pointer_buffer_size = *(u16*)resp.data;

    return 0;
}

void nnServiceClose(Service* s) {
    void* base = nn::sf::hipc::GetMessageBufferOnTls();

    if (s->own_handle || s->object_id) {
        cmifMakeCloseRequest(base, s->own_handle ? 0 : s->object_id);
        nn::sf::hipc::SendSyncRequest(s->session, base, 0x100);
        if (s->own_handle) nn::sf::hipc::CloseClientSessionHandle(s->session);
    }
    *s = (Service){};
}

namespace proc_handle {
    
    namespace {
        
        Handle s_Handle = INVALID_HANDLE;
 
        void ReceiveProcessHandleThreadMain(void *session_handle_ptr) {
            // Convert the argument to a handle we can use.
            Handle session_handle = (Handle)(uintptr_t)session_handle_ptr;
 
            // Receive the request from the client thread.
            memset(armGetTls(), 0, 0x10);
            s32 idx = 0;
            R_ABORT_UNLESS(svcReplyAndReceive(&idx, &session_handle, 1, INVALID_HANDLE, UINT64_MAX));
 
            // Set the process handle.
            s_Handle = ((u32 *)armGetTls())[3];
 
            // Close the session.
            svcCloseHandle(session_handle);
 
            // Terminate ourselves.
            svcExitThread();
 
            // This code will never execute.
            while (true);
        }
 
        void GetViaIpcTrick(void) {
            alignas(PAGE_SIZE) u8 temp_thread_stack[0x1000];
 
            // Create a new session to transfer our process handle to ourself
            Handle server_handle, client_handle;
            R_ABORT_UNLESS(svcCreateSession(&server_handle, &client_handle, 0, 0));
 
            // Create a new thread to receive our handle.
            Handle thread_handle;
            R_ABORT_UNLESS(svcCreateThread(&thread_handle, (void*) &ReceiveProcessHandleThreadMain, (void *)(uintptr_t)server_handle, temp_thread_stack + sizeof(temp_thread_stack), 0x20, 3));
 
            // Start the new thread.
            R_ABORT_UNLESS(svcStartThread(thread_handle));
 
            // Send the message.
            static const u32 SendProcessHandleMessage[4] = { 0x00000000, 0x80000000, 0x00000002, CUR_PROCESS_HANDLE };
            memcpy(armGetTls(), SendProcessHandleMessage, sizeof(SendProcessHandleMessage));
            svcSendSyncRequest(client_handle);
 
            // Close the session handle.
            svcCloseHandle(client_handle);
 
            // Wait for the thread to be done.
            R_ABORT_UNLESS(svcWaitSynchronizationSingle(thread_handle, UINT64_MAX));
 
            // Close the thread handle.
            svcCloseHandle(thread_handle);
        }
 
        Result GetViaMesosphere() {
            R_TRY(svcGetInfo((u64*)&s_Handle, 65001, INVALID_HANDLE, 0));
 
            return 0;
        }
    }
 
    Handle Get() {
        if(s_Handle == INVALID_HANDLE) {
            /* Try to ask mesosphere for our process handle. */
            Result r = GetViaMesosphere();
 
            /* Fallback to an IPC trick if mesosphere is old/not present. */
            if(R_FAILED(r))
                GetViaIpcTrick();
        }
        return s_Handle;
    }
};

}  // namespace skyline::utils
