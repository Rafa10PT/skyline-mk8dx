// Copyright 2017 plutoo
#include "skyline/nx/kernel/thread.h"

#include <string.h>

#include "../internal.h"
#include "alloc.h"
#include "mem.h"
#include "skyline/nx/kernel/svc.h"
#include "skyline/nx/kernel/virtmem.h"
#include "skyline/nx/result.h"
#include "types.h"

extern const u8 __tdata_lma[];
extern const u8 __tdata_lma_end[];
extern u8 __tls_start[];
extern u8 __tls_end[];

// Thread creation args; keep this struct's size 16-byte aligned
typedef struct {
    Thread* t;
    ThreadFunc entry;
    void* arg;
    struct _reent* reent;
    void* tls;
    void* padding;
} ThreadEntryArgs;

static void _EntryWrap(ThreadEntryArgs* args) {
    // Initialize thread vars
    ThreadVars* tv = getThreadVars();
    tv->magic = THREADVARS_MAGIC;
    tv->thread_ptr = args->t;
    tv->reent = args->reent;
    tv->tls_tp = (u8*)args->tls - 2 * sizeof(void*);  // subtract size of Thread Control Block (TCB)
    tv->handle = args->t->handle;

    // Launch thread entrypoint
    args->entry(args->arg);
    svcExitThread();
}

Result threadCreate(Thread* t, ThreadFunc entry, void* arg, void* stack, size_t stack_sz, int prio, int cpuid) {
    return 0;
}

Result threadStart(Thread* t) { return svcStartThread(t->handle); }

Result threadWaitForExit(Thread* t) { return svcWaitSynchronizationSingle(t->handle, -1); }

Result threadClose(Thread* t) {
    Result rc;

    rc = svcUnmapMemory(t->stack_mirror, t->stack_mem, t->stack_sz);
    virtmemFree(t->stack_mirror, t->stack_sz);
    free(t->stack_mem);
    svcCloseHandle(t->handle);

    return rc;
}

Result threadPause(Thread* t) { return svcSetThreadActivity(t->handle, 1); }

Result threadResume(Thread* t) { return svcSetThreadActivity(t->handle, 0); }