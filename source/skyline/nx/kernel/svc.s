.macro SVC_BEGIN name
	.section .text.\name, "ax", %progbits
	.global \name
	.type \name, %function
	.align 2
	.cfi_startproc
\name:
.endm

.macro SVC_END
	.cfi_endproc
.endm

SVC_BEGIN svcSetHeapSize
	PUSH {R0}
	SVC 0x1
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcSetMemoryPermission
	SVC 0x2
	BX LR
SVC_END

SVC_BEGIN svcSetMemoryAttribute
	SVC 0x3
	BX LR
SVC_END

SVC_BEGIN svcMapMemory
	SVC 0x4
	BX LR
SVC_END

SVC_BEGIN svcUnmapMemory
	SVC 0x5
	BX LR
SVC_END

SVC_BEGIN svcQueryMemory
	PUSH {R1}
	SVC 0x6
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcExitProcess
	SVC 0x7
	BX LR
SVC_END

SVC_BEGIN svcCreateThread
	PUSH {R0, R4}
	LDR R0, [SP, #0x8]
	LDR R4, [SP, #0xC]
	SVC 0x8
	POP {R2, R4}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcStartThread
	SVC 0x9
	BX LR
SVC_END

SVC_BEGIN svcExitThread
	SVC 0xA
	BX LR
SVC_END

SVC_BEGIN svcSleepThread
	SVC 0xB
	BX LR
SVC_END

SVC_BEGIN svcGetThreadPriority
	PUSH {R0}
	SVC 0xC
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcSetThreadPriority
	SVC 0xD
	BX LR
SVC_END

SVC_BEGIN svcGetThreadCoreMask	
	PUSH {R0, R1, R4}
	SVC 0xE
	POP {R4}
	STR R1, [R4]
	POP {R4}
	STR R2, [R4]
	STR R3, [R4, #0x4]
	POP {R4}
	BX LR
SVC_END

SVC_BEGIN svcSetThreadCoreMask
	SVC 0xF
	BX LR
SVC_END

SVC_BEGIN svcGetCurrentProcessorNumber
	SVC 0x10
	BX LR
SVC_END

SVC_BEGIN svcSignalEvent
	SVC 0x11
	BX LR
SVC_END

SVC_BEGIN svcClearEvent
	SVC 0x12
	BX LR
SVC_END

SVC_BEGIN svcMapSharedMemory
	SVC 0x13
	BX LR
SVC_END

SVC_BEGIN svcUnmapSharedMemory
	SVC 0x14
	BX LR
SVC_END

SVC_BEGIN svcCreateTransferMemory
	PUSH {R0}
	SVC 0x15
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcCloseHandle
	SVC 0x16
	BX LR
SVC_END

SVC_BEGIN svcResetSignal
	SVC 0x17
	BX LR
SVC_END

SVC_BEGIN svcWaitSynchronization
	PUSH {R0}
	LDR R0, [SP, #0x4]
	LDR R3, [SP, #0x8]
	SVC 0x18
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcCancelSynchronization
	SVC 0x19
	BX LR
SVC_END

SVC_BEGIN svcArbitrateLock
	SVC 0x1A
	BX LR
SVC_END

SVC_BEGIN svcArbitrateUnlock
	SVC 0x1B
	BX LR
SVC_END

SVC_BEGIN svcWaitProcessWideKeyAtomic
	PUSH {R4}
	LDR R3, [SP, #0x4]
	LDR R4, [SP, #0x8]
	SVC 0x1C
	POP {R4}
	BX LR
SVC_END

SVC_BEGIN svcSignalProcessWideKey
	SVC 0x1D
	BX LR
SVC_END

SVC_BEGIN svcGetSystemTick
	SVC 0x1E
	BX LR
SVC_END

SVC_BEGIN svcConnectToNamedPort
	PUSH {R0}
	SVC 0x1F
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcSendSyncRequest
	SVC 0x21
	BX LR
SVC_END

SVC_BEGIN svcSendSyncRequestWithUserBuffer
	SVC 0x22
	BX LR
SVC_END

SVC_BEGIN svcSendAsyncRequestWithUserBuffer
	PUSH {R0}
	SVC 0x23
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcGetProcessId
	PUSH {R0, R4}
	SVC 0x24
	POP {R4}
	STR R1, [R4]
	STR R2, [R4, #0x4]
	POP {R4}
	BX LR
SVC_END

SVC_BEGIN svcGetThreadId
	PUSH {R0, R4}
	SVC 0x25
	POP {R4}
	STM R4, {R1, R2}
	POP {R4}
	BX LR
SVC_END

SVC_BEGIN svcBreak
	SVC 0x26
	BX LR
SVC_END

SVC_BEGIN svcOutputDebugString
	SVC 0x27
	BX LR
SVC_END

SVC_BEGIN svcReturnFromException
	SVC 0x28
	BX LR
SVC_END

SVC_BEGIN svcGetInfo
	PUSH {R0}
	LDR R0, [SP, #0x4]
	LDR R3, [SP, #0x8]
	SVC 0x29
	POP {R3}
	STM R3, {R1, R2}
	BX LR
SVC_END

SVC_BEGIN svcMapPhysicalMemory
	SVC 0x2C
	BX LR
SVC_END

SVC_BEGIN svcUnmapPhysicalMemory
	SVC 0x2D
	BX LR
SVC_END

SVC_BEGIN svcGetResourceLimitLimitValue
	PUSH {R0, R4}
	SVC 0x30
	POP {R4}
	STM R4, {R1, R2}
	POP {R4}
	BX LR
SVC_END

SVC_BEGIN svcGetResourceLimitCurrentValue
	PUSH {R0, R4}
	SVC 0x31
	POP {R4}
	STM R4, {R1, R2}
	POP {R4}
	BX LR
SVC_END

SVC_BEGIN svcSetThreadActivity
	SVC 0x32
	BX LR
SVC_END

SVC_BEGIN svcGetThreadContext3
	SVC 0x33
	BX LR
SVC_END

SVC_BEGIN svcCreateSession
	PUSH {R0, R1}
	SVC 0x40
	POP {R3}
	STR R1, [R3]
	POP {R3}
	STR R2, [R3]
	BX LR
SVC_END

SVC_BEGIN svcAcceptSession
	PUSH {R0}
	SVC 0x41
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcReplyAndReceive
	PUSH {R0, R4}
	LDR R0, [SP, #0x8]
	LDR R4, [SP, #0xC]
	SVC 0x43
	POP {R2}
	STR R1, [R2]
	POP {R4}
	BX LR
SVC_END

SVC_BEGIN svcReplyAndReceiveWithUserBuffer
	PUSH {R0, R4-R6}
	LDR R0, [SP, #0x10]
	LDR R4, [SP, #0x14]
	LDR R5, [SP, #0x18]
	LDR R6, [SP, #0x1C]
	SVC 0x44
	POP {R2}
	STR R1, [R2]
	POP {R4-R6}
	BX LR
SVC_END

SVC_BEGIN svcCreateEvent
	PUSH {R0, R1}
	SVC 0x45
	POP {R3}
	STR R1, [R3]
	POP {R3}
	STR R2, [R3]
	BX LR
SVC_END

SVC_BEGIN svcMapPhysicalMemoryUnsafe
	SVC 0x48
	BX LR
SVC_END

SVC_BEGIN svcUnmapPhysicalMemoryUnsafe
	SVC 0x49
	BX LR
SVC_END

SVC_BEGIN svcSetUnsafeLimit
	SVC 0x4A
	BX LR
SVC_END

SVC_BEGIN svcCreateCodeMemory
	PUSH {R0}
	SVC 0x4B
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcControlCodeMemory
	PUSH {R4-R6}
	LDR R4, [SP, #0xC]
	LDR R5, [SP, #0x10]
	LDR R6, [SP, #0x14]
	SVC 0x4C
	POP {R4-R6}
	BX LR
SVC_END

SVC_BEGIN svcReadWriteRegister
	PUSH {R0}
	LDR R0, [SP, #0x4]
	LDR R1, [SP, #0x8]
	SVC 0x4E
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcCreateSharedMemory
	PUSH {R0}
	SVC 0x50
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcMapTransferMemory
	SVC 0x51
	BX LR
SVC_END

SVC_BEGIN svcUnmapTransferMemory
	SVC 0x52
	BX LR
SVC_END

SVC_BEGIN svcCreateInterruptEvent
	PUSH {R0}
	SVC 0x53
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcQueryPhysicalAddress
	PUSH {R0, R4}
	SVC 0x54
	POP {R4}
	STM R4, {R1, R2, R3}
	POP {R4}
	BX LR
SVC_END

SVC_BEGIN svcQueryIoMapping
	PUSH {R0}
	LDR R0, [SP, #4]
	SVC 0x55
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcCreateDeviceAddressSpace
	PUSH {R0}
	LDR R0, [SP, #0x4]
	LDR R1, [SP, #0x8]
	SVC 0x56
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcAttachDeviceAddressSpace
	SVC 0x57
	BX LR
SVC_END

SVC_BEGIN svcDetachDeviceAddressSpace
	SVC 0x58
	BX LR
SVC_END

SVC_BEGIN svcMapDeviceAddressSpaceByForce
	PUSH {R4-R7}
	LDR R4, [SP, #0x10]
	LDR R5, [SP, #0x14]
	LDR R6, [SP, #0x18]
	LDR R7, [SP, #0x1C]
	SVC 0x59
	POP {R4-R7}
	BX LR
SVC_END

SVC_BEGIN svcMapDeviceAddressSpaceAligned
	PUSH {R4-R7}
	LDR R4, [SP, #0x10]
	LDR R5, [SP, #0x14]
	LDR R6, [SP, #0x18]
	LDR R7, [SP, #0x1C]
	SVC 0x5A
	POP {R4-R7}
	BX LR
SVC_END

SVC_BEGIN svcUnmapDeviceAddressSpace
	PUSH {R4-R6}
	LDR R4, [SP, #0xC]
	LDR R5, [SP, #0x10]
	LDR R6, [SP, #0x14]
	SVC 0x5C
	POP {R4-R6}
	BX LR
SVC_END

SVC_BEGIN svcDebugActiveProcess
	PUSH {R0}
	SVC 0x60
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcBreakDebugProcess
	SVC 0x61
	BX LR
SVC_END

SVC_BEGIN svcTerminateDebugProcess
	SVC 0x62
	BX LR
SVC_END

SVC_BEGIN svcGetDebugEvent
	SVC 0x63
	BX LR
SVC_END

SVC_BEGIN svcLegacyContinueDebugEvent
	SVC 0x64
	BX LR
SVC_END

SVC_BEGIN svcContinueDebugEvent
	SVC 0x64
	BX LR
SVC_END

SVC_BEGIN svcGetProcessList
	PUSH {R0}
	SVC 0x65
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcGetThreadList
	PUSH {R0}
	SVC 0x66
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcGetDebugThreadContext
	PUSH {R4}
	LDR R4, [SP, #0x4]
	SVC 0x67
	POP {R4}
	BX LR
SVC_END

SVC_BEGIN svcSetDebugThreadContext
	PUSH {R4}
	LDR R1, [SP, #0x4]
	LDR R4, [SP, #0x8]
	SVC 0x68
	POP {R4}
	BX LR
SVC_END

SVC_BEGIN svcQueryDebugProcessMemory
	PUSH {R1}
	SVC 0x69
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcReadDebugProcessMemory
	SVC 0x6A
	BX LR
SVC_END

SVC_BEGIN svcWriteDebugProcessMemory
	SVC 0x6B
	BX LR
SVC_END

SVC_BEGIN svcGetDebugThreadParam
	PUSH {R0, R1, R4}
	LDR R0, [SP, #0xC]
	LDR R1, [SP, #0x10]
	LDR R3, [SP, #0x14]
	SVC 0x6D
	POP {R4}
	STM R4, {R1, R2}
	POP {R4}
	STR R3, [R4]
	POP {R4}
	BX LR
SVC_END

SVC_BEGIN svcGetSystemInfo
	PUSH {R0}
	SVC 0x6F
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcCreatePort
	PUSH {R0, R1}
	LDR R0, [SP, #8]
	SVC 0x70
	POP {R3}
	STR R0, [R3]
	POP {R3}
	STR R1, [R3]
	BX LR
SVC_END

SVC_BEGIN svcManageNamedPort
	PUSH {R0}
	SVC 0x71
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcConnectToPort
	PUSH {R0}
	SVC 0x72
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcSetProcessMemoryPermission
	PUSH {R4, R5}
	LDR R1, [SP, #0x8]
	LDR R4, [SP, #0xC]
	LDR R5, [SP, #0x10]
	SVC 0x73
	POP {R4, R5}
	BX LR
SVC_END

SVC_BEGIN svcMapProcessMemory
	PUSH {R4}
	LDR R4, [SP, #0x4]
	SVC 0x74
	POP {R4}
	BX LR
SVC_END

SVC_BEGIN svcUnmapProcessMemory
	PUSH {R4}
	LDR R4, [SP, #0x4]
	SVC 0x75
	POP {R4}
	BX LR
SVC_END

SVC_BEGIN svcQueryProcessMemory
	PUSH {R1}
	LDR R1, [SP, #0x4]
	LDR R3, [SP, #0x8]
	SVC 0x76
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcMapProcessCodeMemory
	PUSH {R4-R6}
	LDR R1, [SP, #0xC]
	LDR R4, [SP, #0x10]
	LDR R5, [SP, #0x14]
	LDR R6, [SP, #0x18]
	SVC 0x77
	POP {R4-R6}
	BX LR
SVC_END

SVC_BEGIN svcUnmapProcessCodeMemory
	PUSH {R4-R6}
	LDR R1, [SP, #0xC]
	LDR R4, [SP, #0x10]
	LDR R5, [SP, #0x14]
	LDR R6, [SP, #0x18]
	SVC 0x78
	POP {R4-R6}
	BX LR
SVC_END

SVC_BEGIN svcCreateProcess
	PUSH {R0}
	SVC 0x79
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcStartProcess
	SVC 0x7A
	BX LR
SVC_END

SVC_BEGIN svcTerminateProcess
	SVC 0x7B
	BX LR
SVC_END

SVC_BEGIN svcGetProcessInfo
	PUSH {R0}
	SVC 0x7C
	POP {R3}
	STR R1, [R3]
	STR R2, [R3, #4]
	BX LR
SVC_END

SVC_BEGIN svcCreateResourceLimit
	PUSH {R0}
	SVC 0x7D
	POP {R2}
	STR R1, [R2]
	BX LR
SVC_END

SVC_BEGIN svcSetResourceLimitLimitValue
	SVC 0x7E
	BX LR
SVC_END
