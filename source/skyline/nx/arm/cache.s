.macro CODE_BEGIN name
	.section .text.\name, "ax", %progbits
	.global \name
	.type \name, %function
	.align 2
	.cfi_startproc
\name:
.endm

.macro CODE_END
	.cfi_endproc
.endm

CODE_BEGIN armDCacheFlush
	push {r4}
	add r1, r1, r0
	mrc p15, 0, r2, c0, c0, 1
	lsr r2, r2, #16
	and r2, r2, #0xf
	mov r3, #4
	lsl r3, r3, r2
	sub r4, r3, #1
	bic r2, r0, r4
	mov r4, r1

armDCacheFlush_L0:
	mcr p15, 0, r2, c7, c14, 1
	add r2, r2, r3
	cmp r2, r4
	bcc armICacheInvalidate_L0

	dsb sy
	pop {r4}
	bx lr
CODE_END

CODE_BEGIN armDCacheClean
	NOP

armDCacheClean_L0:
	NOP
CODE_END

CODE_BEGIN armICacheInvalidate
	push {r4}
	add r1, r1, r0
	mrc p15, 0, r2, c0, c0, 1
	and r2, r2, #0xf
	mov r3, #4
	lsl r3, r3, r2
	sub r4, r3, #1
	bic r2, r0, r4
	mov r4, r1

armICacheInvalidate_L0:
	mcr p15, 0, r2, c7, c5, 1
	add r2, r2, r3
	cmp r2, r4
	bcc armICacheInvalidate_L0

	dsb sy
	pop {r4}
	bx lr
CODE_END

CODE_BEGIN armDCacheZero
	NOP

armDCacheZero_L0:
	NOP
CODE_END