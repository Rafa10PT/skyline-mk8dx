/*
 *  @date   : 2018/04/18
 *  @author : Rprop (r_prop@outlook.com)
 *  https://github.com/Rprop/And64InlineHook
 */
/*
 MIT License

 Copyright (c) 2018 Rprop (r_prop@outlook.com)

 Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all
 copies or substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 SOFTWARE.
 */
#define __STDC_FORMAT_MACROS
#include <inttypes.h>

#include "nn/os.h"
#include "skyline/inlinehook/And32InlineHook.hpp"
#include "skyline/utils/cpputils.hpp"
#include "skyline/nx/arm/cache.h"

#include "skyline/inlinehook/trampoline.hpp"

#define A32_MAX_INSTRUCTIONS 2
#define A32_MAX_REFERENCES (A32_MAX_INSTRUCTIONS * 2)
#define A32_NOP 0xd503201fu
typedef uint32_t* __restrict* __restrict instruction;
typedef struct {
    struct fix_info {
        uint32_t* bprx;
        uint32_t* bprw;
        uint32_t ls;  // left-shift counts
        uint32_t ad;  // & operand
    };
    struct insns_info {
        union {
            uint32_t insu;
            int32_t ins;
            void* insp;
        };
        fix_info fmap[A32_MAX_REFERENCES];
    };
    int32_t basep;
    int32_t endp;
    insns_info dat[A32_MAX_INSTRUCTIONS];

   public:
    inline bool is_in_fixing_range(const int32_t absolute_addr) {
        return absolute_addr >= this->basep && absolute_addr < this->endp;
    }
    inline intptr_t get_ref_ins_index(const int32_t absolute_addr) {
        return static_cast<intptr_t>((absolute_addr - this->basep) / sizeof(uint32_t));
    }
    inline intptr_t get_and_set_current_index(uint32_t* __restrict inp, uint32_t* __restrict outp) {
        intptr_t current_idx = this->get_ref_ins_index(reinterpret_cast<int64_t>(inp));
        this->dat[current_idx].insp = outp;
        return current_idx;
    }
    inline void reset_current_ins(const intptr_t idx, uint32_t* __restrict outp) { this->dat[idx].insp = outp; }
    void insert_fix_map(const intptr_t idx, uint32_t* bprw, uint32_t* bprx, uint32_t ls = 0u,
                        uint32_t ad = 0xffffffffu) {
        for (auto& f : this->dat[idx].fmap) {
            if (f.bprw == NULL) {
                f.bprw = bprw;
                f.bprx = bprx;
                f.ls = ls;
                f.ad = ad;
                return;
            }  // if
        }
        // What? GGing..
    }
    void process_fix_map(const intptr_t idx) {
        for (auto& f : this->dat[idx].fmap) {
            if (f.bprw == NULL) break;
            *(f.bprw) =
                *(f.bprx) | (((int32_t(this->dat[idx].ins - reinterpret_cast<int32_t>(f.bprx) - 8) >> 2) << f.ls) & f.ad);
            f.bprw = NULL;
            f.bprx = NULL;
        }
    }
} context;

//-------------------------------------------------------------------------

static constexpr uint_fast32_t mask = 0x00ffffffu;  // 0b00000000111111111111111111111111

static constexpr int32_t A32MaxValue() {
    return (mask >> 1) * 4 + 8;
}

static constexpr int32_t A32MinValue() {
    return -(mask & ~(mask >> 1)) * 4 + 8;
}

static constexpr ALWAYS_INLINE bool A32BranchReachable(int32_t addr) {
    constexpr auto lower = A32MinValue();
    constexpr auto upper = A32MaxValue();
    return addr >= lower && addr <= upper;
}

//-------------------------------------------------------------------------

static bool __fix_branch_imm(instruction inprwp, instruction inprxp, instruction outprw, instruction outprx,
                             context* ctxp) {
    static constexpr uint32_t mbits = 8u;
    static constexpr uint32_t mask = 0xFF000000u;   // 0b11111111000000000000000000000000
    static constexpr uint32_t rmask = 0x00FFFFFFu;  // 0b00000000111111111111111111111111
    static constexpr uint32_t op_b = 0xEA000000u;   // "b"  ADDR_PCREL26
    static constexpr uint32_t op_bl = 0xEB000000u;  // "bl" ADDR_PCREL26

    const uint32_t ins = *(*inprwp);
    const uint32_t opc = ins & mask;
    switch (opc) {
        case op_b:
        case op_bl: {
            intptr_t current_idx = ctxp->get_and_set_current_index(*inprxp, *outprx);
            int32_t absolute_addr = reinterpret_cast<int32_t>(*inprxp) + 8 +
                                    (static_cast<int32_t>(ins << mbits) >> (mbits - 2u));  // sign-extended
            int32_t new_pc_offset =
                static_cast<int32_t>(absolute_addr - reinterpret_cast<int32_t>(*outprx) - 8) >> 2;  // shifted
            bool special_fix_type = ctxp->is_in_fixing_range(absolute_addr);
            // whether the branch should be converted to absolute jump
            if (!special_fix_type && !A32BranchReachable(new_pc_offset)) {
                if (opc == op_b) {
                    (*outprw)[0] = 0xE51FF004; // LDR PC, [PC, #-4]
                    (*outprw)[1] = absolute_addr;
                    *outprx += 2;
                    *outprw += 2;
                } else {
                    (*outprw)[0] = 0xE28FE004; // ADR LR, #4
                    (*outprw)[1] = 0xE51FF004; // LDR PC, [PC, #-4]
                    (*outprw)[2] = absolute_addr;
                    *outprx += 3;
                    *outprw += 3;
                }  // if
            } else {
                if (special_fix_type) {
                    intptr_t ref_idx = ctxp->get_ref_ins_index(absolute_addr);
                    if (ref_idx <= current_idx) {
                        new_pc_offset =
                            static_cast<int32_t>(ctxp->dat[ref_idx].ins - reinterpret_cast<int32_t>(*outprx) - 8) >> 2;
                    } else {
                        ctxp->insert_fix_map(ref_idx, *outprw, *outprx, 0u, rmask);
                        new_pc_offset = 0;
                    }  // if
                }      // if

                (*outprw)[0] = opc | (new_pc_offset & ~mask);
                ++(*outprw);
                ++(*outprx);
            }  // if

            ++(*inprxp);
            ++(*inprwp);
            return ctxp->process_fix_map(current_idx), true;
        }
    }
    return false;
}

//-------------------------------------------------------------------------

static bool __fix_cond_branch(instruction inprwp, instruction inprxp, instruction outprw, instruction outprx,
                                        context* ctxp) {
    static constexpr uint32_t mbits = 6u;
    static constexpr uint32_t mask = 0x0F000000u;   // 0b00001111000000000000000000000000
    static constexpr uint32_t cond_mask = 0xF0000000u;   // 0b11110000000000000000000000000000
    static constexpr uint32_t rmask = 0x00FFFFFFu;  // 0b00000000111111111111111111111111
    static constexpr uint32_t op_b = 0x0A000000u;   // "b"  ADDR_PCREL26
    static constexpr uint32_t op_bl = 0x0B000000u;  // "bl" ADDR_PCREL26

    const uint32_t ins = *(*inprwp);
    const uint32_t opc = ins & mask;
    const uint32_t cond = ins & cond_mask;
    if(cond == 0xF0000000) return false;
    switch(opc) {
        case op_b:
        case op_bl: {
            intptr_t current_idx = ctxp->get_and_set_current_index(*inprxp, *outprx);
            int32_t absolute_addr = reinterpret_cast<int32_t>(*inprxp) + 8 +
                                    (static_cast<int32_t>(ins << mbits) >> (mbits - 2u));  // sign-extended
            int32_t new_pc_offset =
                static_cast<int32_t>(absolute_addr - reinterpret_cast<int32_t>(*outprx) - 8) >> 2;  // shifted
            bool special_fix_type = ctxp->is_in_fixing_range(absolute_addr);
            // whether the branch should be converted to absolute jump
            if (!special_fix_type && !A32BranchReachable(new_pc_offset)) {
                if (opc == op_b) {
                    (*outprw)[0] = cond | op_b; // B.C #0x8
                    (*outprw)[1] = 0xEA000001;  // B #0xC
                    (*outprw)[2] = 0xE51FF004;  // LDR PC, [PC, #-4]
                    (*outprw)[3] = absolute_addr;
                    *outprx += 4;
                    *outprw += 4;
                } else {
                    (*outprw)[0] = cond | op_b; // B.C #0x8
                    (*outprw)[1] = 0xEA000001;  // B #0x10
                    (*outprw)[2] = 0xE28FE004;  // ADR LR, #4
                    (*outprw)[3] = 0xE51FF004;  // LDR PC, [PC, #-4]
                    (*outprw)[4] = absolute_addr;
                    *outprx += 5;
                    *outprw += 5;
                }  // if
            } else {
                if (special_fix_type) {
                    intptr_t ref_idx = ctxp->get_ref_ins_index(absolute_addr);
                    if (ref_idx <= current_idx) {
                        new_pc_offset =
                            static_cast<int32_t>(ctxp->dat[ref_idx].ins - reinterpret_cast<int32_t>(*outprx) - 8) >> 2;
                    } else {
                        ctxp->insert_fix_map(ref_idx, *outprw, *outprx, 0u, rmask);
                        new_pc_offset = 0;
                    }  // if
                }      // if

                (*outprw)[0] = opc | (new_pc_offset & rmask);
                ++(*outprw);
                ++(*outprx);
            }  // if

            ++(*inprxp);
            ++(*inprwp);
            return ctxp->process_fix_map(current_idx), true;
        }
    }

    return false;
}

//-------------------------------------------------------------------------
#pragma GCC push_options
#pragma GCC optimize ("-O2")
static bool __fix_loadlit(instruction inprwp, instruction inprxp, instruction outprw, instruction outprx,
                          context* ctxp) {

    /* == 0x4100000 ldr(b) and and ldr(b)t */
    static constexpr uint32_t ldr_val       = 0x4100000;    // 0b00000100000100000000000000000000
    static constexpr uint32_t ldr_mask      = 0xC100000;    // 0b00001100010100000000000000000000
    
    /* == 0x1000B0 ldrh and ldrht */
    static constexpr uint32_t ldrh_val      = 0x1000B0;     // 0b00000000000100000000000010110000
    static constexpr uint32_t ldrh_mask     = 0xC5000F0;    // 0b00001100010100000000000011110000
    
    static constexpr uint32_t ldm_val       = 0x8100000;    // 0b00001000000100000000000000000000
    static constexpr uint32_t ldm_mask      = 0xE500000;    // 0b00001110010100000000000000000000
    
    static constexpr uint32_t ldrd_val      = 0xD0;         // 0b00000000000000000000000011010000
    static constexpr uint32_t ldrd_mask     = 0x60000F0;    // 0b00000000010000000000000011010000

    static constexpr uint32_t ldrex_val     = 0x1900090;    // 0b00000001100100000000000010010000
    static constexpr uint32_t ldrex_mask    = 0xF9000F0;    // 0b00001111111100000000000011110000

    /* == 0x1000D0 ldrs[bh] and ldrs[bh]t */
    static constexpr uint32_t ldrs_val      = 0x1000D0;     // 0b00000000000100000000000011010000
    static constexpr uint32_t ldrs_mask     = 0xE1000D0;    // 0b00001110000100000000000011110000

    static constexpr uint32_t pc_reg        = 0xF;          // 0b00000000000000000000000000001111
    static constexpr uint32_t lr_reg        = 0xE;          // 0b00000000000000000000000000001110
    static constexpr uint32_t pc_chk        = 0x000F0000;   // 0b00000000000011110000000000000000

    const uint32_t ins = *(*inprwp);
    if ((ins & 0xF0000000u) == 0xF0000000u || (ins & pc_chk) != (pc_reg << 16)) {
        return false;
    }

    if((ins & ldr_mask) != ldr_val
    && (ins & ldrh_mask) != ldrh_val
    && (ins & ldm_mask) != ldm_val
    && (ins & ldrd_mask) != ldrd_val
    && (ins & ldrex_mask) != ldrex_val
    && (ins & ldrs_mask) != ldrs_val) {
        return false;
    }

    const uint32_t new_ins = (ins & ~pc_chk) | (lr_reg << 16);
    const uint32_t original_pc = reinterpret_cast<uint32_t>(*outprx);

    (*outprw)[0] = 0xE59FE00C; // LDR LR, [PC, #0xC]
    (*outprw)[1] = new_ins;    // new_ins
    (*outprw)[2] = 0xEAFFFFFF; // B 0x4
    (*outprw)[3] = original_pc;

    *outprw += 4;
    *outprx += 4;

    ++(*inprxp);
    ++(*inprwp);
    return true;

}

#pragma GCC pop_options

//-------------------------------------------------------------------------

static bool __fix_pcreladdr(instruction inprwp, instruction inprxp, instruction outprw, instruction outprx,
                            context* ctxp) {
    // Load a PC-relative address into a register
    // http://infocenter.arm.com/help/topic/com.arm.doc.100069_0608_00_en/pge1427897645644.html
    static constexpr uint32_t msb = 8u;
    static constexpr uint32_t lsb = 5u;
    static constexpr uint32_t mask = 0x9f000000u;     // 0b10011111000000000000000000000000
    static constexpr uint32_t rmask = 0x0000001fu;    // 0b00000000000000000000000000011111
    static constexpr uint32_t lmask = 0xff00001fu;    // 0b11111111000000000000000000011111
    static constexpr uint32_t fmask = 0x00ffffffu;    // 0b00000000111111111111111111111111
    static constexpr uint32_t max_val = 0x001fffffu;  // 0b00000000000111111111111111111111
    static constexpr uint32_t op_adr = 0x10000000u;   // "adr"  Rd, ADDR_PCREL21
    static constexpr uint32_t op_adrp = 0x90000000u;  // "adrp" Rd, ADDR_ADRP

    const uint32_t ins = *(*inprwp);
    intptr_t current_idx;
    switch (ins & mask) {
        case op_adr: {
            current_idx = ctxp->get_and_set_current_index(*inprxp, *outprx);
            int64_t lsb_bytes = static_cast<uint32_t>(ins << 1u) >> 30u;
            int64_t absolute_addr = reinterpret_cast<int64_t>(*inprxp) +
                                    (((static_cast<int32_t>(ins << msb) >> (msb + lsb - 2u)) & ~3u) | lsb_bytes);
            int64_t new_pc_offset = static_cast<int64_t>(absolute_addr - reinterpret_cast<int64_t>(*outprx));
            bool special_fix_type = ctxp->is_in_fixing_range(absolute_addr);
            if (!special_fix_type && llabs(new_pc_offset) >= (max_val >> 1)) {
                if ((reinterpret_cast<uint64_t>(*outprx + 2) & 7u) != 0u) {
                    (*outprw)[0] = A32_NOP;
                    ctxp->reset_current_ins(current_idx, ++(*outprx));
                    ++*(outprw);
                }  // if

                (*outprw)[0] = 0x58000000u | (((8u >> 2u) << lsb) & ~mask) | (ins & rmask);  // LDR #0x8
                (*outprw)[1] = 0x14000003u;                                                  // B #0xc
                memcpy(*outprw + 2, &absolute_addr, sizeof(absolute_addr));
                *outprw += 4;
                *outprx += 4;
            } else {
                if (special_fix_type) {
                    intptr_t ref_idx = ctxp->get_ref_ins_index(absolute_addr & ~3ull);
                    if (ref_idx <= current_idx) {
                        new_pc_offset =
                            static_cast<int64_t>(ctxp->dat[ref_idx].ins - reinterpret_cast<int64_t>(*outprx));
                    } else {
                        ctxp->insert_fix_map(ref_idx, *outprw, *outprx, lsb, fmask);
                        new_pc_offset = 0;
                    }  // if
                }      // if

                // the lsb_bytes will never be changed, so we can use lmask to keep it
                (*outprw)[0] = (static_cast<uint32_t>(new_pc_offset << (lsb - 2u)) & fmask) | (ins & lmask);
                ++(*outprw);
                ++(*outprx);
            }  // if
        } break;
        case op_adrp: {
            current_idx = ctxp->get_and_set_current_index(*inprxp, *outprw);
            int32_t lsb_bytes = static_cast<uint32_t>(ins << 1u) >> 30u;
            int64_t absolute_addr =
                (reinterpret_cast<int64_t>(*inprxp) & ~0xfffll) +
                ((((static_cast<int32_t>(ins << msb) >> (msb + lsb - 2u)) & ~3u) | lsb_bytes) << 12);
            skyline::logger::s_Instance->LogFormat("[And32InlineHook] ins = 0x%.8X, pc = %p, abs_addr = %p", ins,
                                                   *inprxp, reinterpret_cast<int64_t*>(absolute_addr));
            if (ctxp->is_in_fixing_range(absolute_addr)) {
                intptr_t ref_idx = ctxp->get_ref_ins_index(absolute_addr /* & ~3ull*/);
                if (ref_idx > current_idx) {
                    // the bottom 12 bits of absolute_addr are masked out,
                    // so ref_idx must be less than or equal to current_idx!
                    skyline::logger::s_Instance->Log(
                        "[And64InlineHook] ref_idx must be less than or equal to current_idx!\n");
                }  // if

                // *absolute_addr may be changed due to relocation fixing
                skyline::logger::s_Instance->Log("What is the correct way to fix this?\n");
                *(*outprw)++ = ins;  // 0x90000000u;
                (*outprx)++;
            } else {
                if ((reinterpret_cast<uint64_t>(*outprx + 2) & 7u) != 0u) {
                    (*outprw)[0] = A32_NOP;
                    ctxp->reset_current_ins(current_idx, ++(*outprx));
                    ++*(outprw);
                }  // if

                (*outprw)[0] = 0x58000000u | (((8u >> 2u) << lsb) & ~mask) | (ins & rmask);  // LDR #0x8
                (*outprw)[1] = 0x14000003u;                                                  // B #0xc
                memcpy(*outprw + 2, &absolute_addr, sizeof(absolute_addr));                  // potential overflow?
                *outprw += 4;
                *outprx += 4;
            }  // if
        } break;
        default:
            return false;
    }

    ctxp->process_fix_map(current_idx);
    ++(*inprxp);
    ++(*inprwp);
    return true;
}

//-------------------------------------------------------------------------

static void __fix_instructions(uint32_t* __restrict inprw, uint32_t* __restrict inprx, int32_t count,
                               uint32_t* __restrict outrwp, uint32_t* __restrict outrxp) {
    context ctx;
    ctx.basep = reinterpret_cast<int64_t>(inprx);
    ctx.endp = reinterpret_cast<int64_t>(inprx + count);
    memset(ctx.dat, 0, sizeof(ctx.dat));
    static_assert(sizeof(ctx.dat) / sizeof(ctx.dat[0]) == A32_MAX_INSTRUCTIONS, "please use A32_MAX_INSTRUCTIONS!");
#ifndef NDEBUG
    if (count > A32_MAX_INSTRUCTIONS) {
        skyline::logger::s_Instance->Log("[And32InlineHook] too many fixing instructions!\n");
    }   // if
#endif  // NDEBUG

    uint32_t* const outprx_base = outrxp;
    uint32_t* const outprw_base = outrwp;

    while (--count >= 0) {
        if (__fix_branch_imm(&inprw, &inprx, &outrwp, &outrxp, &ctx)) continue;
        if (__fix_cond_branch(&inprw, &inprx, &outrwp, &outrxp, &ctx)) continue;
        if (__fix_loadlit(&inprw, &inprx, &outrwp, &outrxp, &ctx)) continue;
        /*if (__fix_pcreladdr(&inprw, &inprx, &outrwp, &outrxp, &ctx)) continue;*/

        // without PC-relative offset
        ctx.process_fix_map(ctx.get_and_set_current_index(inprx, outrxp));
        *(outrwp++) = *(inprw++);
        outrxp++;
        inprx++;
    }

    outrwp[0] = 0xE51FF004u; // LDR PC, [PC, #-4]
    outrwp[1] = reinterpret_cast<int64_t>(inprx);

    /*
    extern Jit _ZL11__insns_jit;
    jitTransitionToWritable(&_ZL11__insns_jit);
    jitTransitionToExecutable(&_ZL11__insns_jit);
    */
}

//-------------------------------------------------------------------------

#define __attribute __attribute__
#define aligned(x) __aligned__(x)
#define __intval(p) reinterpret_cast<intptr_t>(p)
#define __uintval(p) reinterpret_cast<uintptr_t>(p)
#define __ptr(p) reinterpret_cast<void*>(p)
#define __page_size PAGE_SIZE
#define __page_align(n) __align_up(static_cast<uintptr_t>(n), __page_size)
#define __ptr_align(x) __ptr(__align_down(reinterpret_cast<uintptr_t>(x), __page_size))
#define __align_up(x, n) (((x) + ((n)-1)) & ~((n)-1))
#define __align_down(x, n) ((x) & -(n))
#define __countof(x) static_cast<intptr_t>(sizeof(x) / sizeof((x)[0]))  // must be signed
#define __atomic_increase(p) __sync_add_and_fetch(p, 1)
#define __sync_cmpswap(p, v, n) __sync_bool_compare_and_swap(p, v, n)
typedef uint32_t insns_t[A32_MAX_BACKUPS][A32_MAX_INSTRUCTIONS * 10u];

constexpr size_t inline_hook_handler_size = 0xC;  // correct if handler size changes
struct PACKED inline_hook_entry {
    std::array<uint8_t, inline_hook_handler_size> handler;
    const void* cur_handler;
    const void* callback;
    const void* trampoline;
};

constexpr size_t inline_hook_size = sizeof(inline_hook_entry);
constexpr size_t inline_hook_count = 0x1000;
constexpr size_t inline_hook_pool_size = inline_hook_size * inline_hook_count;

//-------------------------------------------------------------------------

static skyline::inlinehook::TrampolineJIT trampoline_jit;
static nn::os::MutexType hookMutex;

//-------------------------------------------------------------------------

void A32HookInit() {
    nn::os::InitializeMutex(&hookMutex, false, 0);

    R_ERRORONFAIL(trampoline_jit.create(sizeof(insns_t), inline_hook_pool_size));
}

//-------------------------------------------------------------------------

static void FastAllocateTrampoline(uint32_t** rx, uint32_t** rw) {
    static_assert((A32_MAX_INSTRUCTIONS * 10 * sizeof(uint32_t)) % 8 == 0, "8-byte align");
    static volatile int32_t __index = -1;

    uint32_t i = __atomic_increase(&__index);
    const auto insns_jit = trampoline_jit.get_insns_jit();
    insns_t* rwptr = (insns_t*)insns_jit.get_rw_addr();
    insns_t* rxptr = (insns_t*)insns_jit.get_rx_addr();
    *rw = (*rwptr)[i];
    *rx = (*rxptr)[i];
}

//-------------------------------------------------------------------------

void* A32HookFunctionV(void* const symbol, void* const replace, void* const rxtr, void* const rwtr,
                       const uintptr_t rwx_size) {

    uint32_t *rxtrampoline = static_cast<uint32_t*>(rxtr), *rwtrampoline = static_cast<uint32_t*>(rwtr),
             *original = static_cast<uint32_t*>(symbol);

    static_assert(A32_MAX_INSTRUCTIONS >= 2, "please fix A32_MAX_INSTRUCTIONS!");
    const auto pc_offset = static_cast<int32_t>(__intval(replace) - __intval(symbol) - 8) >> 2;
    if (!A32BranchReachable(pc_offset)) {
        skyline::inlinehook::ControlledPages control(original, 5 * sizeof(uint32_t));
        /* Map as writable for W^X. */
        control.claim();

        /* Move to RW addr. */
        original = (u32*)control.rw;

        constexpr u32 count = 2;
        if (rxtrampoline) {
            if (rwx_size < count * 10u) {
                skyline::logger::s_Instance->LogFormat(
                    "[And32InlineHook] rwx size is too small to hold %u bytes backup instructions!", count * 10u);
                control.unclaim();
                return NULL;
            }  // if
            /* Fix what's about to happen because the og instructions will disappear and we have trampoline. */
            __fix_instructions(original, (u32*)control.rx, count, rwtrampoline, rxtrampoline);
        }  // if

        /* Overwrite. */
        original[0] = 0xE51FF004; // LDR PC, [PC, #-4]
        original[1] = __intval(replace);
        //__flush_cache(symbol, 2 * sizeof(uint32_t));

        skyline::logger::s_Instance->LogFormat(
            "[And32InlineHook] inline hook %p->%p successfully! %zu bytes overwritten", symbol, replace,
            2 * sizeof(uint32_t));

        /* Unmap as writable for W^X. */
        control.unclaim();
    } else {
        constexpr u32 count = 1;
        skyline::inlinehook::ControlledPages control(original, count * sizeof(uint32_t));
        control.claim();

        original = (u32*)control.rw;

        if (rwtrampoline) {
            /*rwx_size = A32_MAX_INSTRUCTIONS (==5) * 10u*/
            if (rwx_size < 1u * 10u) {
                skyline::logger::s_Instance->LogFormat(
                    "[And32InlineHook] rwx size is too small to hold %u bytes backup instructions!", count * 10u);
                control.unclaim();
                return NULL;
            }  // if
            __fix_instructions(original, (u32*)control.rx, count, rwtrampoline, rxtrampoline);
        }  // if


        //usar
        __sync_cmpswap(original, *original, 0xEA000000u | (static_cast<u32>(pc_offset) & mask));  // "B" ADDR_PCREL26
        //__flush_cache(symbol, 1 * sizeof(uint32_t));

        skyline::logger::s_Instance->LogFormat(
            "[And32InlineHook] inline hook %p->%p successfully! %zu bytes overwritten", symbol, replace,
            1 * sizeof(uint32_t));

        control.unclaim();
    }  // if

    // if(rwtrampoline)
    //    rwtrampoline[0] = 0xDEADBEEF;
    return rxtrampoline;
}

//-------------------------------------------------------------------------

extern "C" void A32HookFunction(void* const symbol, void* const replace, void** result) {
    nn::os::LockMutex(&hookMutex);

    const auto insns_jit = trampoline_jit.get_insns_jit();

    R_ERRORONFAIL(insns_jit.write_mode());

    uint32_t *rxtrampoline = NULL, *rwtrampoline = NULL;
    if (result != NULL) {
        FastAllocateTrampoline(&rxtrampoline, &rwtrampoline);
        *result = rxtrampoline;
        if (rxtrampoline == NULL) {
            *(u32*)rxtrampoline = 0;
            return;
        };
    }  // if


    rxtrampoline =
        (uint32_t*)A32HookFunctionV(symbol, replace, rxtrampoline, rwtrampoline, A32_MAX_INSTRUCTIONS * 10u);
    if (rxtrampoline == NULL && result != NULL) {
        *result = NULL;

    }  // if

    R_ERRORONFAIL(insns_jit.executable_mode());

    nn::os::UnlockMutex(&hookMutex);
}

extern const void (*inlineHandlerStart)(void);
extern const void* inlineHandlerEnd;
extern const void (*inlineHandlerImpl)(void);

u64 inline_hook_curridx = 0;

extern "C" void A32InlineHook(void* const address, void* const callback) {
    u32 handler_start_addr = (u32)&inlineHandlerStart;
    u32 handler_end_addr = (u32)&inlineHandlerEnd;

    // make sure inline hook handler constexpr is correct
    if (inline_hook_handler_size != handler_end_addr - handler_start_addr) {
        skyline::logger::s_Instance->LogFormat("[A32InlineHook] invalid handler size, mannual updating required");
        R_ERRORONFAIL(MAKERESULT(Module_Skyline, SkylineError_InlineHookHandlerSizeInvalid));
    }

    // check pool availability
    if (inline_hook_curridx >= inline_hook_count) {
        skyline::logger::s_Instance->LogFormat("[A32InlineHook] inline hook pool exausted");
        R_ERRORONFAIL(MAKERESULT(Module_Skyline, SkylineError_InlineHookPoolExhausted));
    }

    // prepare to hook
    const auto inline_hook_jit = trampoline_jit.get_inline_hook_jit();
    inline_hook_jit.executable_mode();
    auto& rx_entries = *reinterpret_cast<std::array<inline_hook_entry, inline_hook_count>*>(inline_hook_jit.get_rx_addr());
    auto& rx = rx_entries[inline_hook_curridx];

    // hook to call the handler
    void* trampoline;
    A32HookFunction(address, &rx.handler, &trampoline);

    // populate handler entry
    inline_hook_jit.write_mode();
    auto& rw_entries = *reinterpret_cast<std::array<inline_hook_entry, inline_hook_count>*>(inline_hook_jit.get_rw_addr());
    auto& rw = rw_entries[inline_hook_curridx];

    memcpy(rw.handler.data(), (void*)handler_start_addr, inline_hook_handler_size);
    rw.cur_handler = &inlineHandlerImpl;
    rw.callback = callback;

    // Fishguy6564 decided to patch this out and execute the instructions by himself.
    // rw.trampoline = trampoline;
    //rw.trampoline = (void*)((char *)trampoline + 4);

    //Nah bro, my solution was way stupider than that (Above edit crashes current plugins)
    rw.trampoline = (void*)((char*)address + 4);

    // finalize, make handler executable
    inline_hook_jit.executable_mode();

    inline_hook_curridx++;
}
