#include "skyline/inlinehook/trampoline.hpp"
#include "skyline/utils/cpputils.hpp"

#include "skyline/nx/kernel/svc.h"
#include "skyline/nx/kernel/virtmem.h"

#include "skyline/nx/runtime/env.h"

namespace skyline::inlinehook {
    namespace {
        Result start_set_trampoline(Trampoline &t, uintptr_t cur_searching_addr, uintptr_t cur_heap_addr, size_t jit_sz) {
                t.set_src_addr(cur_heap_addr);
                const size_t sz = ALIGN_UP(jit_sz, PAGE_SIZE);
                t.set_size(sz);

                // search for applicable space for inline hook JIT
                cur_searching_addr =
                    static_cast<uintptr_t>(cur_searching_addr - sz);  // start searching from right before .text


                // Yes, this does find a place before main/rtld.
                MemoryInfo mem;
                while (true) {
                    u32 page_info;
                    if (R_SUCCEEDED(svcQueryMemory(&mem, &page_info, cur_searching_addr)) && mem.type == MemType_Unmapped &&
                        mem.size >= sz) {
                        break;
                    }
                    cur_searching_addr -= PAGE_SIZE;
                }

                t.set_rx_addr(mem.addr + mem.size - sz);

                Result rc = svcMapProcessCodeMemory(envGetOwnProcessHandle(), t.get_rx_addr(), t.get_src_addr(), sz);

                if(R_SUCCEEDED(rc)) {
                    rc = svcSetProcessMemoryPermission(envGetOwnProcessHandle(), t.get_rx_addr(), sz, Perm_Rx);
                }

                return rc;
        }

        Result set_trampoline_write(Trampoline &t) {
            t.set_rw_addr(reinterpret_cast<uintptr_t>(virtmemReserve(t.get_size())));
            return svcMapProcessMemory(reinterpret_cast<void *>(t.get_rw_addr()),
            envGetOwnProcessHandle(), t.get_rx_addr(), t.get_size());
        }
    }

    Result TrampolineJIT::create(size_t insns_jit_sz, size_t inline_hook_jit_sz) {
        Result rc = 0;
        MemoryInfo mem_info;
        u32 dummy;
        u32 address_check = utils::g_MainHeapAddr;
        size_t final_size = 0;
        while(true) {
            rc = svcQueryMemory(&mem_info, &dummy, address_check);
            if (R_FAILED(rc) || (mem_info.addr != address_check) || (mem_info.type != MemType_Heap))
                break;
            final_size += mem_info.size;
            address_check += mem_info.size;
        }
        final_size = ALIGN_UP(final_size, 0x200000);
        if(R_SUCCEEDED(rc)) {
            void *heap;
            rc = svcSetHeapSize(&heap, final_size +
            ALIGN_UP(insns_jit_sz + inline_hook_jit_sz, 0x200000));

            if(R_SUCCEEDED(rc)) {
                const auto cur_addr = utils::g_MainHeapAddr + final_size;
                rc = start_set_trampoline(this->__inline_hook_jit,
                skyline::utils::g_RtldTextAddr,  // start searching from right before .text
                static_cast<uintptr_t>(cur_addr), inline_hook_jit_sz);

                if(R_FAILED(rc)) return rc;

                start_set_trampoline(this->__insns_jit,
                this->__inline_hook_jit.get_rx_addr(),
                static_cast<uintptr_t>(cur_addr + inline_hook_jit_sz), insns_jit_sz);

                if(R_FAILED(rc)) return rc;

                rc = set_trampoline_write(__insns_jit);
                
                if(R_FAILED(rc)) return rc;

                rc = set_trampoline_write(__inline_hook_jit);

                if(R_FAILED(rc)) return rc;
            }
        }

        return rc;
    }

    Result Trampoline::executable_mode() const {
        Result rc;

        rc = svcUnmapProcessCodeMemory(envGetOwnProcessHandle(), this->rx_addr, this->src_addr, size);

        if(R_SUCCEEDED(rc)) {
            rc = svcMapProcessCodeMemory(envGetOwnProcessHandle(), this->rx_addr, this->src_addr, size);
        
            if(R_SUCCEEDED(rc)) {
                rc = svcSetProcessMemoryPermission(envGetOwnProcessHandle(), this->rx_addr, this->size, Perm_Rx);
            }
        }

        return rc;
    }

}
