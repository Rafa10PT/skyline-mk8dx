.section ".text.crt0","ax"
.global __module_start
.extern __nx_module_runtime

__module_start:
    b startup
    .word __nx_mod0 - __module_start

.org __module_start+0x80
startup:
    // save lr
    MOV  R6, LR

    // get aslr base
    bl   ____aslr__
    ____aslr__: SUB  R7, LR, #0x88

    // context ptr and main thread handle
    MOV  R4, R0
    MOV  R5, R1
bssclr_start:

    // clear .bss
    @ADR R0, __bss_start__
    @ADR R1, __bss_end__
    @ADD R0, R0, #:lo12:__bss_start__
    @ADD R1, R1, #:lo12:__bss_end__
    SUB R1, R1, R0  // calculate size
    ADD R1, R1, #7  // round up to 8
    BIC R1, R1, #7

bss_loop:
	MOV R8, #0
    STR R8, [R0], #8
    SUBS R1, R1, #8
    BNE  bss_loop

    // store stack pointer
    MOV  R1, SP
    @ADR R0, __stack_top
    @STR  R1, [R0, #:lo12:__stack_top]

    // initialize system
    MOV R0, R4
    MOV R1, R5
    MOV R2, R6
	.word 0xdeadbeef
.section ".rodata.mod0"
.global __nx_mod0
__nx_mod0:
    .ascii "MOD0"
    .word  __dynamic_start__    - __nx_mod0
    .word  __bss_start__        - __nx_mod0
    .word  __bss_end__          - __nx_mod0
    .word  0
    .word  0
    .word  __nx_module_runtime  - __nx_mod0
