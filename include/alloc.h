/**
 * @file alloc.h
 * @brief Allocation functions.
 */

#pragma once

#include "types.h"

#ifdef __cplusplus
extern "C" {
#endif

void* malloc(size_t size);
void free(void* src);
void* calloc(size_t num, size_t size);
void* realloc(void* ptr, size_t size);
void* aligned_alloc(size_t alignment, size_t size);
u64 malloc_usable_size(void* ptr);

#ifdef __cplusplus
}
#endif