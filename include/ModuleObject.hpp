#pragma once

#include <assert.h>
#include <elf.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

namespace rtld {
struct ModuleObject {
   private:
    // ResolveSymbols internals
    inline void ResolveSymbolRelAbsolute(Elf64_Rel* entry);
    inline void ResolveSymbolRelaAbsolute(Elf64_Rela* entry);
    inline void ResolveSymbolRelJumpSlot(Elf64_Rel* entry, bool do_lazy_got_init);
    inline void ResolveSymbolRelaJumpSlot(Elf64_Rela* entry, bool do_lazy_got_init);

   public:
    struct ModuleObject* next;
    struct ModuleObject* prev;
    union {
        Elf32_Rel* rel;
        Elf32_Rela* rela;
        void* raw;
    } rela_or_rel_plt;
    union {
        Elf32_Rel* rel;
        Elf32_Rela* rela;
    } rela_or_rel;
    char *module_base;
    Elf32_Dyn* dynamic;
    bool is_rela;
    size_t rela_or_rel_plt_size;
    void (*dt_init)(void);
    void (*dt_fini)(void);
    uint32_t* hash_bucket;
    uint32_t* hash_chain;
    char* dynstr;
    Elf32_Sym* dynsym;
    size_t dynstr_size;
    void** got;
    size_t rela_dyn_size;
    size_t rel_dyn_size;
    size_t rel_count;
    size_t rela_count;
    size_t hash_nchain_value;
    size_t hash_nbucket_value;
    void *got_stub_ptr;
#ifdef __RTLD_6XX__
    size_t soname_idx;
    size_t nro_size;
    bool cannot_revert_symbols;
#endif

    void Initialize(uint64_t aslr_base, Elf64_Dyn* dynamic);
    void Relocate();
    Elf64_Sym* GetSymbolByName(const char* name);
    void ResolveSymbols(bool do_lazy_got_init);
    bool TryResolveSymbol(Elf64_Addr* target_symbol_address, Elf64_Sym* symbol);
};

#ifdef __RTLD_6XX__
//static_assert(sizeof(ModuleObject) == 0xD0, "ModuleObject size isn't valid"); <- find for Aarch32
#else
//static_assert(sizeof(ModuleObject) == 0xB8, "ModuleObject size isn't valid"); <- find for Aarch32
#endif
}  // namespace rtld