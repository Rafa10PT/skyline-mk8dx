#pragma once

#include "types.h"

namespace skyline::inlinehook {
class Trampoline {
   private:
    size_t size;
    uintptr_t src_addr;
    uintptr_t rx_addr;
    uintptr_t rw_addr;

   public:
    constexpr ALWAYS_INLINE size_t get_size() const { return size; }
    constexpr ALWAYS_INLINE uintptr_t get_src_addr() const { return src_addr; }
    constexpr ALWAYS_INLINE uintptr_t get_rx_addr() const { return rx_addr; }
    constexpr ALWAYS_INLINE uintptr_t get_rw_addr() const { return rw_addr; }

    constexpr ALWAYS_INLINE void set_size(size_t size) { this->size = size; }
    constexpr ALWAYS_INLINE void set_src_addr(uintptr_t src_addr) { this->src_addr = src_addr; }
    constexpr ALWAYS_INLINE void set_rx_addr(uintptr_t rx_addr) { this->rx_addr = rx_addr; }
    constexpr ALWAYS_INLINE void set_rw_addr(uintptr_t rw_addr) { this->rw_addr = rw_addr; }

    constexpr ALWAYS_INLINE Result write_mode() const { return 0; }
    Result executable_mode() const;
};

class TrampolineJIT {
    private:
        /* Trampoline to still be able to call functions that 
         * have been hooked.
         */
        Trampoline __insns_jit;
        /* Trampoline for inline hooks. */
        Trampoline __inline_hook_jit;

    public:
        Result create(size_t insns_jit_sz, size_t inline_hook_jit_sz);

        constexpr ALWAYS_INLINE Trampoline &get_insns_jit() {
            return __insns_jit;
        }

        constexpr ALWAYS_INLINE Trampoline &get_inline_hook_jit() {
            return __inline_hook_jit;
        }
};

};  // namespace skyline::inlinehook