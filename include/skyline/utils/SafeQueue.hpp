#pragma once

#include <functional>
#include <memory>
#include <queue>

#include "mem.h"
#include "nn/os.h"
#include "skyline/logger/Logger.hpp"
#include "types.h"

namespace skyline::utils {

class Task {
   public:
    std::function<void()> taskFunc;
    nn::os::EventType completionEvent;

    Task();
    Task(std::function<void()> taskFunc);

    ~Task();
};

template <typename T>
class SafeQueue {
   private:
    T** buffer;

   protected:
    nn::os::MessageQueueType queue;

   public:
    SafeQueue(u64 count) {
        buffer = (T**)malloc(sizeof(T*) * count);
        nn::os::InitializeMessageQueue(&queue, reinterpret_cast<std::size_t *>(buffer), sizeof(T*) * count);
    }

    void push(std::unique_ptr<T>* ptr) { nn::os::SendMessageQueue(&queue, *reinterpret_cast<std::size_t *>(&ptr)); }
    bool push(std::unique_ptr<T>* ptr, nn::TimeSpan span) {
        return nn::os::TimedSendMessageQueue(&queue, reinterpret_cast<std::size_t>(ptr), span);
    }

    void pop(std::unique_ptr<T>** ptr) { nn::os::ReceiveMessageQueue(reinterpret_cast<std::size_t *>(ptr), &queue); }
    bool pop(std::unique_ptr<T>** ptr, nn::TimeSpan span) {
        return nn::os::TimedReceiveMessageQueue(reinterpret_cast<std::size_t *>(ptr), &queue, span);
    }

    bool tryPush(std::unique_ptr<T>* ptr) { return nn::os::TrySendMessageQueue(&queue, reinterpret_cast<std::size_t>(ptr)); }
    bool tryPop(std::unique_ptr<T>** ptr) { return nn::os::TryReceiveMessageQueue(reinterpret_cast<std::size_t *>(ptr), &queue); }

    ~SafeQueue() {
        nn::os::FinalizeMessageQueue(&queue);
        delete[] buffer;
    }
};

class SafeTaskQueue : public SafeQueue<Task> {
   public:
    nn::os::ThreadType thread;

    SafeTaskQueue(u64);

    void startThread(s32 priority, s32 core, u64 stackSize);
    void _threadEntrypoint();
};
};  // namespace skyline::utils