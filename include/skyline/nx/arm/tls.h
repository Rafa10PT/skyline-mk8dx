/**
 * @file tls.h
 * @brief AArch64 thread local storage.
 * @author plutoo
 * @copyright libnx Authors
 */
#pragma once
#include "types.h"

/**
 * @brief Gets the thread local storage buffer.
 * @return The thread local storage buffer.
 */
static inline void* armGetTls(void) {
    void *reg;
    asm volatile("mrc p15, 0, %[data], c13, c0, 3" : [ data ] "=r"(reg));
    return reg;
}
