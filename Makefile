# TODO (Khangaroo): Make this process a lot less hacky (no, export did not work)
# See MakefileNSO

.PHONY: all clean skyline send

TURBOVER ?= 171

PYTHON := python3
ifeq (, $(shell which python3))
	# if no python3 alias, fall back to `python` and hope it's py3
	PYTHON   := python
endif

NAME 			:= $(shell basename $(CURDIR))
NAME_LOWER		:= $(shell echo $(NAME) | tr A-Z a-z)
PATCH_PREFIX	:= $(NAME_LOWER)_patch_
PATCH 			:= $(PATCH_PREFIX)$(TURBOVER)

PATCH_DIR 		:= patches
SCRIPTS_DIR		:= scripts
BUILD_DIR 		:= build$(TURBOVER)

CONFIGS 		:= $(PATCH_DIR)/configs
TURBO_CONFIG 	:= $(CONFIGS)/$(TURBOVER).config

MAPS 			:= $(PATCH_DIR)/maps
TURBO_MAPS 		:= $(MAPS)/$(TURBOVER)
NAME_MAP 		:= $(BUILD_DIR)/$(NAME)$(TURBOVER).map

GEN_PATCH		:= $(SCRIPTS_DIR)/genPatch.py
SEND_PATCH		:= $(SCRIPTS_DIR)/sendPatch.py

MAKE_NSO		:= nso.mk

all: skyline

skyline:
	$(MAKE) all -f $(MAKE_NSO) MAKE_NSO=$(MAKE_NSO) TURBOVER=$(TURBOVER) BUILD=$(BUILD_DIR) TARGET=$(NAME)$(TURBOVER)
	#$(MAKE) $(PATCH)/*.ips

$(PATCH)/*.ips: $(PATCH_DIR)/*.slpatch $(TURBO_CONFIG) $(TURBO_MAPS)/*.map $(NAME_MAP) 
	@rm -f $(PATCH)/*.ips
	$(PYTHON) $(GEN_PATCH) $(TURBOVER)

send: all
	$(PYTHON) $(SEND_PATCH) $(IP) $(TURBOVER)

clean:
	$(MAKE) clean -f $(MAKE_NSO)
	@rm -fr $(PATCH_PREFIX)*
